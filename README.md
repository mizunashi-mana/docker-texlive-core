# Docker Image for TeXLive

This is minimal.  I recommend to use [full packages image](https://gitlab.com/mizunashi-mana/docker-texlive)

## Usage

```bash
docker login registry.gitlab.com
docker run -it registry.gitlab.com/mizunashi-mana/docker-texlive-core sh
```
